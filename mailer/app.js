'use strict';

const MailgunService = require('./lib/mailgun');
const SendgridService = require('./lib/sendgrid');

//  To-do
// let config = require('./config');

String.prototype.capitalizeFirstLetter = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

// Instance services
let servicesInstances = {
  MailgunService: MailgunService,
  SendgridService: SendgridService
};

class MailerService {
  constructor(config) {
    config = config || {};

    // TO-DO
    // move config to config json
    this.supportedSupport = ['sendgrid', 'mailgun'];
    this.config = {
      services: {
        mailgun: {
          apiKey: '',
          domain: ''
        },
        sendgrid: {
          apiKey: ''
        }
      },
      servicesFailoverOrder: this.supportedSupport,
      retryTimes: 3 // check how many times before failover
    };

    if (typeof config === 'object') {
      this.config = Object.assign({}, this.config, config);
    } else {
      throw new Error('config should be an Object type');
    }

    let $config = this.config;
    this.config.servicesFailoverOrder.forEach(function(value, index, servicesList) {
      value = value.toLowerCase();
      $config.servicesFailoverOrder[index] = value;

      if (!$config.services[value]) {
        throw new Error(`Provided support ${value} is not supported`);
      }
    });
  }

  /**
   *
   * @param serviceName
   * @param data
   * @returns {Promise}
   */
  sendViaService(serviceName, data) {
    return new Promise((resolve, reject) => {
      console.log(`Sending via ${serviceName}...`);
      let serviceToCall = `${serviceName.capitalizeFirstLetter()}Service`;
      let Service = new servicesInstances[serviceToCall](this.config.services[serviceName]);
      Service.send(data.from, data.to, data.cc, data.bcc, data.subject, data.message)
        .then(s => {
          console.log('Emails was successfully sent');
          resolve(s);
        })
        .catch(e => {
          console.log(`Fail to send via ${serviceName}`);
          reject(e);
        });
    });
  }

  /**
   * Send a email through the send mail service
   *
   * @param from
   * @param to
   * @param cc
   * @param bcc
   * @param subject
   * @param message
   * @returns {Promise}
   */
  send(from, to, cc, bcc, subject, message) {
    let $config = this.config;
    let retryTimesIndex = 1;
    let serviceIndex = 0;

    let data = {
      from: from,
      to: to,
      cc: cc,
      bcc: bcc,
      subject: subject,
      message: message
    };

    return new Promise((resolve, reject) => {
      const failOverHandler = e => {
        console.log(
          `Failover:: serviceIndex: ${serviceIndex}, retryTimesIndex: ${retryTimesIndex}, error: ${e.message}`
        );

        ++serviceIndex;
        if (serviceIndex > this.config.servicesFailoverOrder.length - 1) {
          serviceIndex = 0;
          ++retryTimesIndex;
        }
        if (retryTimesIndex > $config.retryTimes) {
          let message = 'The message was not sent, all the send mail services are unavailable';
          reject(message);
        }
        this.sendViaService(this.config.servicesFailoverOrder[serviceIndex], data)
          .then(resolve)
          .catch(failOverHandler);
      };

      this.sendViaService(this.config.servicesFailoverOrder[serviceIndex], data)
        .then(resolve)
        .catch(failOverHandler);
    });
  }
}
// let email = new MailerService();
module.exports = MailerService;
