const assert = require('assert');

const Mailgun = require('../lib/mailgun');
const config = require('../config');

describe('Mailgun tests', function() {
  describe('#init & send', function() {
    it('should throw error if config not proper set', function() {
      assert.throws(function() {
        let mailer = new Mailgun({ domain: 'test' });
      });
    });
    it('should not throw error on proper config provided', function() {
      let mailer = new Mailgun({
        apiKey: config.IncorrectApiKey,
        domain: 'no-reply.com'
      });
    });
    it('should Forbidden error when not proper apiKey', function(done) {
      const resolvingPromise = new Promise((resolve, reject) => {
        let mailer = new Mailgun({
          apiKey: config.IncorrectApiKey,
          domain: 'no-reply.com'
        });

        mailer
          .send(
            config.sendFromEmail,
            config.sendToEmail,
            config.sendCcEmail,
            config.sendBccEmail,
            'Subject from Mailgun',
            'Some content here'
          )
          .then(body => resolve(body))
          .catch(e => reject(e));
      });

      resolvingPromise
        .catch(e => {
          assert.equal(e.message, 'Forbidden', 'Mailgun apiKey|domain are forbidden');
        })
        .then(done, done);
    });

    it('should send successfully the mail', function(done) {
      const resolvingPromise = new Promise((resolve, reject) => {
        let mailer = new Mailgun({
          apiKey: config.mailgunApikey,
          domain: config.mailgunDomain
        });

        mailer
          .send(
            config.sendFromEmail,
            config.sendToEmail,
            config.sendCcEmail,
            config.sendBccEmail,
            'Subject from Mailgun',
            'Some content here'
          )
          .then(body => resolve(body))
          .catch(e => reject(e));
      });

      resolvingPromise
        .then(result => {
          assert.ok(result);
        })
        .then(done, done);
    });
  });
});
