const assert = require('assert');
const Sendgrid = require('../lib/sendgrid');
const config = require('../config');

describe('Sendgrid tests', function() {
  describe('#init & send', function() {
    it('should throw error if config not proper set', function() {
      assert.throws(function() {
        let mailer = new Sendgrid();
      });
    });
    it('should not throw error on proper config provided', function() {
      let mailer = new Sendgrid({
        apiKey: config.IncorrectApiKey
      });
    });
    it('should error when not proper apiKey', function(done) {
      const resolvingPromise = new Promise((resolve, reject) => {
        let mailer = new Sendgrid({
          apiKey: config.IncorrectApiKey
        });

        mailer
          .send(
            config.sendFromEmail,
            config.sendToEmail,
            config.sendCcEmail,
            config.sendBccEmail,
            'Subject from Sendgrid',
            'Some content here'
          )
          .then(body => resolve(body))
          .catch(e => reject(e));
      });

      resolvingPromise
        .then(result => {})
        .catch(e => {
          assert.equal(e.message, 'Response error');
        })
        .then(done, done);
    });

    it('should send successfully the mail', function(done) {
      const resolvingPromise = new Promise((resolve, reject) => {
        let mailer = new Sendgrid({
          apiKey: config.sendgridApikey
        });

        mailer
          .send(
            config.sendFromEmail,
            config.sendToEmail,
            config.sendCcEmail,
            config.sendBccEmail,
            'Subject from Sendgrid',
            'Some content here'
          )
          .then(body => resolve(body))
          .catch(e => reject(e));
      });

      resolvingPromise
        .then(result => {
          assert.ok(result);
        })
        .then(done, done);
    });
  });
});
