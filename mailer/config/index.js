const env = process.env.DEPLOY_ENV || 'LOCAL';
const config = require(`./${env}.json`);
config.env = env;

module.exports = config;
