'use strict';

const sgMail = require('sendgrid').mail;
const sg = require('sendgrid');

class Sendgrid {
  constructor(config) {
    this.apiKey = config.apiKey || null;
  }

  send(from, to, ccs, bccs, subject, message) {
    let content_text = new sgMail.Content('text/plain', message);
    let email_to = new sgMail.Email(to);
    let email_cc = new sgMail.Email(ccs);
    let email_bcc = new sgMail.Email(bccs);
    let email_from = new sgMail.Email(from);
    let personalization = new sgMail.Personalization();

    // to-do add multiple receipents for email
    // if (typeof to === array) {
    //   to.forEach(element => {
    //     email_to = new sgMail.Email(element);
    //     personalization.addTo(email_to);
    //   });
    // } else {
    //   personalization.addTo(to);
    // }

    personalization.addTo(email_to);
    personalization.addCc(email_cc);
    personalization.addBcc(email_bcc);
    personalization.setSubject(subject);

    let mail = new sgMail.Mail(email_from, subject, email_to, content_text);
    mail.addPersonalization(personalization);

    // setting apikey here
    let sendGrid = sg(this.apiKey);

    // sending email request
    let request = sendGrid.emptyRequest({
      method: 'POST',
      path: '/v3/mail/send',
      body: mail.toJSON()
    });

    return new Promise((resolve, reject) => {
      sendGrid
        .API(request)
        .then(resolve)
        .catch(error => {
          // logging error and adding perfixes sendgrid
          console.log(`Error in Sendgrid while sending:  ${error.message}, `, error.response.body.errors);
          reject(error);
        });
    });
  }
}
// let testemail = new Sendgrid();

module.exports = Sendgrid;
