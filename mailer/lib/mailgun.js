'use strict';

const MailgunSDK = require('mailgun-js');

class Mailgun {
  constructor(config) {
    this.apiKey = config.apiKey || null;
    this.domain = config.domain || null;

    // check if apikey is null or length invalid
    if (this.apiKey === null || this.apiKey.length <= 10) {
      throw new Error('API key should be provided for mailgun service');
    }

    // check if domain is null or length invalid
    if (this.domain === null || this.domain.length <= 3) {
      throw new Error('Domain should be provided for mailgun');
    }
  }

  send(from, to, cc, bcc, subject, message) {
    // basic data object with required fields
    // message can be plain
    let data = {
      from: from,
      to: to,
      cc: cc,
      bcc: bcc,
      subject: subject,
      text: message
    };
    let mailgun = new MailgunSDK({ apiKey: this.apiKey, domain: this.domain });

    return new Promise((resolve, reject) => {
      mailgun.messages().send(data, function(err, body) {
        if (err) {
          // logging error and adding perfixes mailgun
          console.log('Error in mailgun: ', err.message);
          reject(err);
        } else {
          resolve(body);
        }
      });
    });
  }
}
// let testemail = new Mailgun();

module.exports = Mailgun;
