const { Validator } = require('node-input-validator');

class CheckEmailValidation {
  matched(data) {
    let v = new Validator({ email: data }, { email: 'required|email' });
    return v.check();
  }
}
module.exports = CheckEmailValidation;
