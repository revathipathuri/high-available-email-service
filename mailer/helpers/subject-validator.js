const { Validator } = require('node-input-validator');

class CheckSubjectValidation {
  matched(data) {
    let v = new Validator({ subject: data }, { subject: 'required|alphaNumeric|minLength:2|maxLength:50' });
    return v.check();
  }
}

module.exports = CheckSubjectValidation;
