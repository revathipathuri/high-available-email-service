const { Validator } = require('node-input-validator');

class CheckMessageValidation {
  matched(data) {
    let v = new Validator({ message: data }, { message: 'required' });
    return v.check();
  }
}
module.exports = CheckMessageValidation;
