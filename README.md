# High Available Email Service

## Overview

Main aim is to create a highly avaliable email service i.e if one email service goes down other will pickup automatically.

Build with two email providers:

- [SendGrid](https://sendgrid.com)
- [Mailgun](http://www.mailgun.com)

### Prerequisites

What things you need to install the software and how to setup your system

```
* Install
	Node JS Latest version 7.x
    Visual studio code

```

### Run this a service

Before start using instal npm package

```
    cd mailer
    npm install
```

For starting the service in mailer folder

```
    npm start
```

## Running the tests

Before runing test fill the config file with data

```

    "sendFromEmail":  "",
    "sendToEmail":    "",
    "mailgunApikey":  "",
    "mailgunDomain":  "",
    "mandrillApikey": "",
    "sendgridApikey": "",
    "wrongApiKey":    ""

```

To run test

```

    cd mailer
    npm test

```

### TODO List

```

    1) Cater for multiple email recipients(Added it for sendgrid but not tested. need to add for mailgun)
    2) Validation on input fields(created helpers need to integrate.)
    3) Build proper config(Created config file, but need to redirect from config)
    4) Extend this to more email providers(plan to extend it for 4 or more.)

```

### Usage

High available service will return promise

```
    let sendService = new MailService({
        services: {
            mailgun: {
                apiKey: 'mailgun-apiKey',
                domain: 'mailgun-domain',
            },
            sendgrid: {
                apiKey: 'sendgrid-apiKey'
            },
            servicesFailoverOrder: {
                'sendgrid',
                'mailgun'
            }
        },
    });

    ...

    sendService.send(fromEmail, toEmail, ccEmail, BccEmail, subject, content)
        .then(...)
        .catch(...)
```

Serivce failover is designed in round robin idea.

## Author

    Revathi Pathuri
     - AWS - Solution architech assoicate
     - Nodejs experenice - 6 years
     - Angular 2+ development experience - 3 years
     - React experenice - 20 months
     - Angular.js - 2years

```

```
